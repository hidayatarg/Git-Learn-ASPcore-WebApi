﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyAngular.Model;
using EasyAngular.Model.Security;
using Microsoft.AspNetCore.Mvc;

namespace EasyAngular.Api.Controllers.Securtiy
{
    [Route("api/[controller]")]
    public class LoginController:Controller
    {
        // POST 
        [HttpPost]
        public JsonResult Login([FromBody]RequestDto<LoginDto> requestOfLogin)
        {
            //db actions
            //simulate db actions
            if (requestOfLogin.Entity.Username == "umut" && requestOfLogin.Entity.Password == "123")
            {
                return Json(new ResponseDto<TokenDto>
                {
                    Entity = new TokenDto { Key = Guid.NewGuid().ToString() }
                });
            }
            else
            {
                return Json(new ResponseDto<TokenDto>
                {
                    HasError = true,
                    Error = "LoginNotFound"
                });
            }

        }
    }
}
