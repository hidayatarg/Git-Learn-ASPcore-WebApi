﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAngular.Model
{
    public class ResponseDto<T> where T:BaseDto
    {
        public T Entity { get; set; }
        public bool HasError { get; set; }
        public string Error { get; set; }
    }
}
