﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAngular.Model.Security
{
    public class TokenDto : BaseDto
    {
        public string Key { get; set; }
    }
}
