﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyAngular.Model
{
   public class RequestDto<T> where T:BaseDto
   {
        public T Entity { get; set; }
    }
}
